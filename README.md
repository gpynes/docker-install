# Docker + Docker-Commpose Install Script
This is a quick install script to get docker and docker-compose on a unix machine. (Only tested on mac and Ubuntu to be honest)

### Getting Started
1. Clone the repo (or just copy the `docker-install.sh` file)
```bash
git clone https://gitlab.com/gpynes/docker-install.git
```

2. Run the script
```bash
sh docker-install/docker-install.sh
```

3. Remove this directory
```bash
rm -rf docker-install
```
<br>

That's it! you should now be able to run `docker` and `docker-compose` enjoy! :)